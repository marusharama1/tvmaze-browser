import ShowsStore from '@/store/modules/shows';

describe('getShowsByGenres', () => {
  const shows = [
    {
      name: 'Pose',
      genres: ['Drama'],
    },
    {
      name: 'Mission impossible',
      genres: ['Action'],
    },
  ];
  const state = {
    shows,
  };

  it('returns all shows', () => {
    const allShows = ShowsStore.getters.getShows(state)();
    expect(allShows).toHaveLength(2);
  });

  it('returns only drama', () => {
    const dramaOnly = ShowsStore.getters.getShows(state)('drama');
    expect(dramaOnly).toHaveLength(1);
    expect(dramaOnly[0].genres).toEqual(expect.arrayContaining(['Drama']));
  });
});
