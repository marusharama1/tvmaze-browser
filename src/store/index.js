/* eslint-disable */
import Vue from 'vue';
import Vuex from 'vuex';
import createLogger from 'vuex/dist/logger';
import shows from './modules/shows';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';
export default new Vuex.Store({

  modules: {
    shows: shows,
  },
  strict: debug,
  plugins: debug ? [createLogger()] : [],
});
