import axios from 'axios';

const state = {
  shows: [],
  selectedShow: {},
  showsSearch: [],
};

const getters = {
  getShows(state) {
    return (genre) => {
      if (!genre) {
        return state.shows;
      }
      return state.shows.filter((show) => {
        if (show.genres
          .map(x => x.toLowerCase())
          .includes(genre.toLowerCase())) {
          return true;
        }
      });
    };
  },
  showsSearch(state) {
    return state.showsSearch;
  },
  selectedShow(state) {
    return state.selectedShow;
  },
  getGenres(state) {
    const genres = new Set();
    state.shows.forEach((x) => {
      x.genres.forEach(y => genres.add(y));
    });
    return Array.from(genres);
  },
};

const actions = {
  getAllShows({ commit }) {
    return axios
      .get('/shows')
      .then(result => result.data)
      .then((data) => {
        commit('SHOWS', data);
      });
  },
  getShowDetails({ commit }, id) {
    return axios
      .get(`/shows/${id}`)
      .then(result => result.data)
      .then((data) => {
        commit('SELECTED_SHOW', data);
      });
  },
  searchShowsByName({ commit }, q) {
    const params = { q };
    return axios
      .get('/search/shows', { params })
      .then(result => result.data)
      .then((data) => {
        commit('SEARCH_RESULTS', data);
      });
  },
};

const mutations = {
  SHOWS(state, shows) {
    state.shows = shows;
  },
  SELECTED_SHOW(state, data) {
    state.selectedShow = data;
  },
  SEARCH_RESULTS(state, search) {
    state.showsSearch = [];
    search.map((item) => {
      state.showsSearch.push(item.show);
    });
  },

};
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
