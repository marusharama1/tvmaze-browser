import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/pages/home';
import Details from '@/pages/details';
import Search from '@/pages/search';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
    },
    {
      path: '/show/:id',
      name: 'Details',
      component: Details,
    },
    {
      path: '/search',
      name: 'search-keyword',
      component: Search,
    },
  ],
});
