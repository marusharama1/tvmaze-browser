import Vue from 'vue';
import axios from 'axios';
import { BootstrapVue } from 'bootstrap-vue';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faStar, faChevronRight, faChevronLeft, faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import VueCarousel from 'vue-carousel';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import App from './App';
import router from './router';
import store from './store';

library.add(faStar, faChevronRight, faChevronLeft, faSearch);
Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.use(BootstrapVue);
Vue.use(VueCarousel);

axios.defaults.baseURL = process.env.BASE_URL; // the prefix of the URL
Vue.config.productionTip = false;

const initApp = new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
});

initApp();
