# Tvmaze browser

This project implements browser by Tvmaze API. It's based on VueJS 2 + Vuex.
Some logic is covered by unit tests.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minificatuion
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

```
## Framework
Project is base on VueJS. I’ve chosen this framework because it’s easy to use, and it’s pretty intuitive, also it has a well-defined ecosystem.
Vuex store is also implemented.
[Framework guide](https://vuejs.org/)
[Store guide](https://vuex.vuejs.org/guide/)

## HTTP requests
For the http client I’ve chosen axios. It based on promises and if the application needs to be extended to SSR it supports it as well.
[Client guide](https://github.com/axios/axios)

## CSS preprocessor
SCSS preprocessor has been used to increase productivity.
[SCSS guide](https://sass-lang.com)

## Responsive Web
For the responsiveness has used Bootstrap-vue. I’ve chosen this framework because it has predefined a grid, generic CSS-classes, a many other elements or features (like image lazy loading).
The framework is very flexible, and you can define which functionality you want to have in the resulting bundle, all styles can be easily overridden if original package needs to be extended.
[Framework guide](https://bootstrap-vue.org/)

## Icons
For icons the Font Awesome npm package has been used, which also very flexible and you can define particular icons which you to have as a part of your end bundle.
[Font Awesome guide](https://fontawesome.com/)

## Img-carousel
For the carousel on the home page Vue-carousel has been used. This plugin is well done, it is responsive and supports swipe events. I chose it for MVP as a quick solution.
[Guide](https://github.com/SSENSE/vue-carousel)

## Eslint
Project is using code quality control tool - Eslint. It is commonly used lately among code linters.
[Guide](https://eslint.org/)

## Jest
Unit tests are implemented with Jest. Complex logic of Vuex store is tested.
[Guide](https://deltice.github.io/jest/)
